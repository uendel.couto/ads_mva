// --------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
// --------------------------------------------------------------------
using namespace std;
// --------------------------------------------------------------------
void save( string str ){
   cout << str;
   ofstream fo;
   replace(str.begin(), str.end(),'.',',');
   fo.open("MVA2-MS.csv");
   fo << str;
   fo.close();
}
int main( void ){
  int     N  = 30;
  double  S0 = 0.000503 ,   V0 = 3,   D0 = S0*V0, //CPU
          S1 = 0.000939 ,   V1 = 1,   D1 = S1*V1, //RAM
          S2 = 0.00125 ,   V2 = 5,   D2 = S2*V2, //Barramento
          S3 = 0.000174,   V3 = 1,   D3 = S3*V3, //Placa de v�deo
          Z  = 0.1     ,

          Q0, Q1, Q2, Q3,
          R0, R1, R2, R3, 
          U0, U1, U2, U3, 

          R, X;

  stringstream ss;
  ss << setprecision(3);
  ss << "n;R0;R1;R2;R3;"
     << "R;X;"
     << "Q0;Q1;Q2;Q3;"
     << "U0;U1;U2;U3;"
     << endl;

  Q0 = Q1 = Q2 = Q3 = 0.0;
  for( int n = 1; n < N; n++ ){
       R0 = S0*(1.0 + Q0);
       R1 = S1*(1.0 + Q1);
       R2 = S2*(1.0 + Q2);
       R3 = S3*(1.0 + Q3);
    

       R  = R0*V0 + R1*V1 + R2*V2 + R3*V3;
       X  = n/(R+Z);

       Q0 = X*R0*V0;
       Q1 = X*R1*V1;
       Q2 = X*R2*V2;
       Q3 = X*R3*V3;

       U0 = Q0/(1+Q0);
       U1 = Q1/(1+Q1);
       U2 = Q2/(1+Q2);
       U3 = Q3/(1+Q3);

       ss << n  << ";"
          << R0 << ";" << R1 << ";" << R2 << ";" << R3 << ";" 
          << R  << ";" << X  << ";"
          << Q0 << ";" << Q1 << ";" << Q2 << ";" << Q3 << ";" 
          << U0 << ";" << U1 << ";" << U2 << ";" << U3 << ";" << endl;
  }
  save(ss.str());
  return 0;
}
